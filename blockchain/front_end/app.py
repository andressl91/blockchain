import time
from functools import wraps
from flask import Flask, render_template, flash, redirect, url_for, session, request, logging
from passlib.hash import sha256_crypt
from flask_mysqldb import MySQL

# Avoiding circular import, using *
from blockchain.front_end.sql_utility import *
from blockchain.front_end.forms import *

# Initiate app
app = Flask(__name__)

app.config["MYSQL_HOST"] = 'localhost'
app.config["MYSQL_USER"] = 'crypto'
app.config["MYSQL_PASSWORD"] = 'crypto'
app.config["MYSQL_DB"] = 'crypto'
app.config["MYSQL_CURSORCLASS"] = 'DictCursor'

mysql = MySQL(app)
# Helper wrapper to aviod access with /dashboard direct url

# Take a look here, copy in https://github.com/willassad/cryptocurrencypython

def is_logged_in(f):
    @wraps(f)
    def wrap(*args, **kwargs):
        if 'logged_in' in session:
            return f(*args, **kwargs)
        else:
            flash("Unauthorized, please login", "danger")
            return redirect(url_for('login'))
    return wrap


def log_in_user(username):
    users = Table("users", "name", "email", "username", "password")
    user = users.getone("username", username)

    session['logged_in'] = True
    session['username'] = username
    session['name'] = user.get('name')
    session['email'] = user.get('email')

@app.route("/register", methods= ['GET', 'POST'])
def register():
    form = RegisterForm(request.form)
    users = Table("users", "name", "email", "username", "password")

    # Verify all fields are validated in RegisterForm
    if request.method == 'POST' and form.validate():
        username = form.username.data
        email = form.email.data
        name = form.name.data

        if isnewuser(username): # Check if user allready exists
            password = sha256_crypt.encrypt(form.password.data)
            users.insert(name,email,username,password)
            log_in_user(username)
            return redirect(url_for('dashboard'))

        else:
            flash("User allready exists", "danger")
            return redirect(url_for("register"))

    # Here form is a reference to our deinfed form, and the form in the register.html
    return render_template('register.html', form=form)

@app.route("/login", methods=['GET', 'POST'])
def login():
    if request.method == 'POST':
        username = request.form['username']
        candidate = request.form['password']

        users = Table("users", "name", "email", "username", "password")
        user = users.getone("username", username)
        accpass = user.get('password')

        if accpass is None:
            flash("Username is not found", 'danger')
            return redirect(url_for('login'))
        else:
            if sha256_crypt.verify(candidate, accpass):
                log_in_user(username)
                flash('You are now logged in', 'success')
                return redirect(url_for('dashboard'))
            else:
                flash("invalid password", "danger")
                return redirect(url_for('login'))
    return render_template('login.html')

@app.route("/transaction", methods=['GET', 'POST'])
@is_logged_in
def transcation():
    form = SendMoneyForm(request.form)
    balance = get_balance(session.get('username'))

    if request.method == 'POST':
        try:
            send_money(sender=session.get('username'),
                       recipient=form.username.data,
                       amount=form.amount.data)
            flash(f"Money sent to recipient {form.username.data}")
        except Exception as e:
            flash(str(e), 'danger')

        return redirect(url_for('transcation'))

    return render_template('transaction.html', balance=balance, form=form, page='transcation')

@app.route("/buy", methods = ['GET', 'POST'])
@is_logged_in
def buy():
    form = BuyForm(request.form)
    balance = get_balance(session.get('username'))

    if request.method == 'POST':
        try:
            send_money("BANK", session.get('username'), form.amount.data)
            flash("Money Sent!", 'Success')
        except Exception as e:
            flash(str(e), 'danger')

        return redirect(url_for('dashboard'))

    return render_template('buy.html', balance=balance, form=form, page='buy')

@app.route("/logout")
@is_logged_in
def logout():
    session.clear()
    flash("Logout success", "success")
    return redirect(url_for('login'))


@app.route("/dashboard")
@is_logged_in
def dashboard():
    blockchain = get_blockchain()
    current_time = time.strftime("%I:%M %p")
    return render_template("dashboard.html", session=session, current_time=current_time, blockchain=blockchain,
                           page='dashboard')

@app.route("/")
def index():
    #blockchain = get_blockchain()
    #number = len(blockchain.chain) + 1
    #data = "%s-->%s-->%s" % ("BANK", "jackal", 100)
    #blockchain.mine(Block(number, data=data))
    #sync_blockchain(blockchain)
    #send_money("jackal", "dover", 5)
    #users = Table("users","name","email","username","password")
    #users.insert("Ola", "ola@dole.com","oladole", "hash")
    #users.drop()
    return render_template("index.html")

if __name__ == "__main__":
    app.secret_key = 'spam'
    app.run(debug=True)