from hashlib import sha256

def update_hash(*args):
    """ Generic hash function

    Args:
        *args: Expects a iterable, where every item is a string type

    Returns:
        sha256 has of input

    """
    hashing_text = ""
    h = sha256()
    for arg in args:
        hashing_text += str(arg)

    h.update(hashing_text.encode('utf-8'))
    return h.hexdigest()


class Block:

    def __init__(self, number=0, previous_hash="0"*64, data=None, nonce=0):
        self.data = data
        self.number = number # block id
        self.nonce = 0
        self.previous_hash = previous_hash # sha256 is fixed 64 characters, default genesis block

    # We don't store hash, but always call hash function
    # Motivation if someone changes the block data in a blockchain, will change the nonce
    # which in turn changes the hash.
    @property
    def hash(self):
        return update_hash(self.previous_hash, self.number, self.data, self.nonce)

    def __str__(self):
        return f"Block={self.number}, hash={self.hash}, previous_hash={self.previous_hash}, " \
               f"data={self.data}, nonce={self.nonce}"


class Blockchain:

    def __init__(self,  difficulty=3):
        self.chain = []
        self.difficulty = difficulty

    def add(self, block: Block):
        """Add new block to new blockchain

        Args:
            block: New block to be added to the blockchain

        Returns:

        """
        self.chain.append(block)

    def remove(self, block):
        self.chain.remove(block)

    def mine(self, block: Block):
        # TODO: Improve handling of genesis block
        if len(self.chain) >= 1:
            block.previous_hash = self.chain[-1].hash
        # Challenge to find a sha256 hash where the first n (difficulty) chars are n*0
        while True:
            # Solved, add block to chain
            if block.hash[:self.difficulty] == "0" * self.difficulty:
                self.add(block)
                break
            # Try new hash, by changing the block nonce
            else:
                block.nonce += 1

    def is_valid(self):
        for i in range(1, len(self.chain)):
            current_block = self.chain[i]
            previous_block = self.chain[i-1]
            # Check legder
            if current_block.previous_hash != previous_block.hash:
                return False
            # Check solved hash
            if current_block.hash[:self.difficulty] != "0" * self.difficulty:
                return False
        return True


def main():
    pass


if __name__ == "__main__":
    main()