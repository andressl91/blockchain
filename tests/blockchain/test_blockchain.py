from blockchain.blockchain import Blockchain, Block
from blockchain.front_end.sql_utility import sync_blockchain
def get_blockchain(difficulty=3):
    blockchain = Blockchain(difficulty=difficulty)
    database = ["hello", "egg", "spam"]

    for i, data in enumerate(database):
        blockchain.mine((Block(number=i+1, data=data))) # First iterable i 0

    for block in blockchain.chain:
        print(block)

    return blockchain

def test_blockchain():
    blockchain = get_blockchain(difficulty=3)

    chain = blockchain.chain
    assert len(chain) == 3
    assert chain[0].previous_hash == "0" * 64
    assert chain[1].previous_hash == chain[0].hash
    assert chain[2].previous_hash == chain[1].hash

    assert blockchain.is_valid()

    # Corrupt chain
    # By changing the data, the nonce used to calculate the hash will change, which in turn
    # will give a different hash
    chain[1].data = "holymoly"
    assert not blockchain.is_valid()
    blockchain.mine(blockchain.chain[1])
    assert not blockchain.is_valid()

def test_sync_blockchain():
    blockchain = get_blockchain()
    sync_blockchain(blockchain)
